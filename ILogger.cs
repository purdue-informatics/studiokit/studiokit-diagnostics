﻿using System.IO;

namespace StudioKit.Diagnostics
{
	public interface ILogger
	{
		void Debug(string message, TextWriter log = null);

		void Debug(string format, params object[] args);

		void Debug(object data);

		void Debug(params object[] data);

		void Info(string message, TextWriter log = null);

		void Info(string format, params object[] args);

		void Info(object data);

		void Info(params object[] data);

		void Warn(string message, TextWriter log = null);

		void Warn(string format, params object[] args);

		void Warn(object data);

		void Warn(params object[] data);

		void Error(string message, TextWriter log = null);

		void Error(string format, params object[] args);

		void Error(object data);

		void Error(params object[] data);

		void Critical(string message, TextWriter log = null);

		void Critical(string format, params object[] args);

		void Critical(object data);

		void Critical(params object[] data);
	}
}